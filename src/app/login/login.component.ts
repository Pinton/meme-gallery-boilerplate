import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  username: FormControl;
  password: FormControl;

  constructor(
    formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.form = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

    this.username = this.form.controls['username'] as FormControl;
    this.password = this.form.controls['password'] as FormControl;
  }

  ngOnInit(): void {}

  async onSubmit(value: any): Promise<void> {
    if (this.form.valid) {
      this.authService.login(value)
        .then(() => this.router.navigate(['home']));
    }
  }

}
