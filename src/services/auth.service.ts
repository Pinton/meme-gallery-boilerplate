import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

interface Credentials {
  username: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  async login(credentials: Credentials): Promise<void> {}
}
